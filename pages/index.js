import { useEffect, useState } from "react";
import Head from "next/head";
import styles from "../style/Home.module.css";
import { Spinner, Row } from "react-bootstrap";

import ArtistCard from "../components/ArtistCard";

export default function Home() {
  const [isLoading, setIsLoading] = useState(true);
  const [artists, setArtists] = useState([]);

  useEffect(() => {
    let requestBody = {
      query: ` query {
                    popular_artists {
                      artists {
                        _id
                        name
                        image {
                          url
                        }
                      }
                    }
                }
            `,
    };
    fetch("https://metaphysics-staging.artsy.net/", {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed");
          setIsLoading(false);
        }
        return res.json();
      })
      .then((resData) => {
        setArtists(resData.data.popular_artists.artists);
        setIsLoading(false);
        console.log(resData.data.popular_artists.artists);
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
      });
  }, []);
  return (
    <div className={styles.container}>
      <Head>
        <title>Artsy</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {isLoading ? (
        <Spinner animation="grow" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      ) : (
        <Row>
          <ArtistCard props={artists} />
        </Row>
      )}
    </div>
  );
}
