import Head from "next/head";
import { Container, Row, Col, Jumbotron, Image } from "react-bootstrap";
import Artworks from "../../components/Artworks";

export default function artist({ artistData }) {
  const { bio, birthday, image, name, nationality } = artistData.data.artist;
  return (
    <>
      <Head>
        <title>Artist: {name} </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Jumbotron>
        <Container>
          <Row>
            <Col xs={12} md={6}>
              <Image src={image.url} rounded className="mx-auto d-block" />
            </Col>
            <Col
              xs={12}
              md={6}
              className="d-flex flex-column justify-content-center mt-3"
            >
              <h4>Artist Name: {name}</h4>
              <p>
                Birthday:{" "}
                {birthday !== ""
                  ? birthday
                  : "No available data at this moment."}
              </p>
              <p>Nationality: {nationality}</p>
              <p>Bio: {bio}</p>
            </Col>
          </Row>
        </Container>
      </Jumbotron>
      <Container>
        <Row>
          <Artworks props={artistData.data.artist.artworks} />
        </Row>
      </Container>
    </>
  );
}

export async function getStaticPaths() {
  let requestBody = {
    query: ` query {
                    popular_artists {
                        artists {
                        _id
                        name
                        image {
                            url
                        }
                        }
                    }
                }
            `,
  };
  const res = await fetch("https://metaphysics-staging.artsy.net/", {
    method: "POST",
    body: JSON.stringify(requestBody),
    headers: {
      "Content-Type": "application/json",
    },
  });

  const artistData = await res.json();
  const paths = artistData.data.popular_artists.artists.map((artist) => {
    return { params: { id: artist._id } };
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  let requestBody = {
    query: ` query($artistId: String!) {
                    artist(id: $artistId) {
                        _id
                        name
                        years
                        birthday
                        bio
                        nationality
                        image {
                          url
                        }
                        artworks {
                          _id
                          title
                          price
                          image {
                            url
                          }
                        }
                    }
                }
            `,
    variables: {
      artistId: params.id,
    },
  };
  const res = await fetch("https://metaphysics-staging.artsy.net/", {
    method: "POST",
    body: JSON.stringify(requestBody),
    headers: {
      "Content-Type": "application/json",
    },
  });

  const artistData = await res.json();

  return {
    props: {
      artistData,
    },
  };
}
