import "../style/custom.css";
import Header from "../components/Header";
import { Container } from "react-bootstrap";

function MyApp({ Component, pageProps }) {
  return (
    <Container>
      <Header />
      <Component {...pageProps} />
    </Container>
  );
}

export default MyApp;
