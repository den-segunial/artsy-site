import React, { useEffect, memo } from "react";
import { Card, Col } from "react-bootstrap";
import Link from "next/link";

const ArtistCard = ({ props }) => {
  return (
    <>
      {props.length !== 0 ? (
        props.map((artist) => {
          return (
            <Col xs={12} md={4} key={artist._id} className="my-3">
              <Card>
                <Card.Img variant="top" src={artist.image.url} />
                <Card.Body>
                  <Card.Title>{artist.name}</Card.Title>
                  <Card.Text>
                    <Link href={`/artist/${artist._id}`}>
                      Click to see more artist artwork.
                    </Link>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          );
        })
      ) : (
        <h1>No Artist are register at this moment</h1>
      )}
    </>
  );
};

export default memo(ArtistCard);
