import React from "react";
import Link from "next/link";
import { Navbar } from "react-bootstrap";

const Header = () => {
  return (
    <Navbar expand="lg">
      <Link href="/">
        <a className="navbar-brand">Artsy: A place for artist</a>
      </Link>
    </Navbar>
  );
};

export default Header;
