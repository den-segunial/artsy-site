import { memo } from "react";
import { Card, Col } from "react-bootstrap";
const Artworks = ({ props }) => {
  console.log(props);
  return (
    <>
      {props.length !== 0 ? (
        props.map((artwork) => {
          return (
            <Col xs={12} md={4} key={artwork._id} className="my-3">
              <Card>
                <Card.Img variant="top" src={artwork.image.url} />
                <Card.Body>
                  <Card.Title>{artwork.title}</Card.Title>
                  <Card.Text>
                    {artwork.price !== ""
                      ? artwork.price
                      : "No Price at this moment"}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          );
        })
      ) : (
        <h1>No Artwork are register at this moment</h1>
      )}
    </>
  );
};

export default memo(Artworks);
